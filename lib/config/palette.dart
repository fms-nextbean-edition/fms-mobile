import 'package:flutter/material.dart';

class Palette {
  static const Color scaffold = Color(0xFFF0F2F5);
  static const Color white = Color.fromRGBO(255, 255, 255, 1);
  static const Color backgroundBody = Color.fromRGBO(250, 250, 250, 1);
  static const Color facebookBlue = Color(0xFF1777F2);
  static const Color border = Color.fromRGBO(228, 228, 231,1);
  static const Color blackText =  Color.fromRGBO(39, 39, 42, 1);
  static const Color greyText = Color.fromRGBO(161, 161, 170,1);
  static const Color green = Color.fromRGBO(22,163,74,1);
  static const LinearGradient createRoomGradient = LinearGradient(
    colors: [Color(0xFF496AE1), Color(0xFFCE48B1)],
  );

  static const Color online = Color(0xFF4BCB1F);

  static const LinearGradient storyGradient = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Colors.transparent, Colors.black26],
  );
}