import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fmsmobile/config/palette.dart';
import 'package:fmsmobile/services/ignore_certificat.dart';
import 'screens/screens.dart';
import 'screens/sign_in_google.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: ".env");

  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/login',
      routes: {
        '/login': (context) => const SignInGoogle(),
        '/home': (context) => NavScreen()
      },
    );
  }
}
