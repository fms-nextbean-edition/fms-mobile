class Account {
  final String id;
  final String lastName;
  final String firstName;
  final String gender;
  final DateTime dob;
  final String address;
  final String? image;
  final String code;
  final bool status;
  final DateTime accessTokenExpiryTime;

  Account({
    required this.id,
    required this.lastName,
    required this.firstName,
    required this.gender,
    required this.dob,
    required this.address,
    required this.image,
    required this.code,
    required this.status,
    required this.accessTokenExpiryTime,
  });

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
      id: json['id'],
      lastName: json['lastName'],
      firstName: json['firstName'],
      gender: json['gender'],
      dob: DateTime.parse(json['dateOfBirth']),
      address: json['address'],
      image: json['image'],
      code: json['code'],
      status: json['isDeleted'],
      accessTokenExpiryTime: DateTime.parse(json['accessTokenExpiryTime']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id':id,
      'lastName': lastName,
      'firstName': firstName,
      'gender': gender,
      'dateOfBirth': dob.toIso8601String(),
      'address': address,
      'image': image,
      'code': code,
      'isDeleted': status,
      'accessTokenExpiryTime': accessTokenExpiryTime.toIso8601String(),
    };
  }
}

