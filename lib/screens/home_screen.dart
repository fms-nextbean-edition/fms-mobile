import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fmsmobile/config/palette.dart';
import 'package:fmsmobile/widgets/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import '../services/storage_service.dart';
import '../models/account.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Account account;
  final TextEditingController searchController = TextEditingController();
  final ValueNotifier<List<String>> suggestionsNotifier = ValueNotifier([]);

  @override
  void initState() {
    super.initState();
    _loadUserData();
  }

  @override
  void dispose() {
    searchController.dispose();
    suggestionsNotifier.dispose();
    super.dispose();
  }

  Future<void> _loadUserData() async {
    final storageService = StorageService();
    final userData = await storageService.getUserData();
    if (userData != null) {
      setState(() {
        account = userData;
      });
    }
    print(account);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.backgroundBody,
      appBar: AppBar(
        title: const Padding(
          padding: EdgeInsets.all(16),
        ),
        backgroundColor: Palette.backgroundBody,
        automaticallyImplyLeading: false,
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Palette.backgroundBody,
            expandedHeight: 56.0,
            floating: true,
            pinned: true,
            automaticallyImplyLeading: false,
            title: Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 24.0, bottom: 16.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      account != null
                          ? 'Hi, ${account.firstName}!'
                          : 'FMSNextBean',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.ibmPlexSans(
                        fontSize: 24.0,
                        height: 1.4,
                        fontWeight: FontWeight.w500,
                        color: Palette.blackText,
                      ),
                    ),
                    Text(
                      'Wanna find some jobs today?',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.ibmPlexSans(
                        fontSize: 16.0,
                        height: 1.5,
                        fontWeight: FontWeight.w400,
                        color: Palette.greyText,
                      ),
                    ),
                  ]),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(padding: EdgeInsets.only(top: 8.0)),
                  CustomSearchBar(
                    controller: searchController,
                    onTap: () {
                      suggestionsNotifier.value = List.generate(5, (index) => 'Item $index');
                    },
                    onChanged: (value) {
                      suggestionsNotifier.value = List.generate(5, (index) => 'Item $index: $value');
                    },
                  ),
//                   Container(
//                      CustomFilterBar(
//                     controller: filterController,

//                   ),
//                   ),
//                   Container(
// CustomProjectBar(
//   controller: projectController,

// )
//                   ),
                
                  const SizedBox(height: 16.0),
                  const Text('Welcome, your App'),
                  const SizedBox(height: 40.0),
                  ElevatedButton(
                    onPressed: () async {
                      final storageService = StorageService();
                      await storageService.clearUserData();
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/login', (route) => false);
                    },
                    child: const Text('Logout'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
