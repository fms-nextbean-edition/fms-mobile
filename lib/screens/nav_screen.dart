import 'package:flutter/material.dart';
import 'package:fmsmobile/config/palette.dart';
import 'package:fmsmobile/screens/screens.dart';
import 'package:fmsmobile/widgets/widgets.dart';
import 'package:lucide_icons/lucide_icons.dart';


class NavScreen extends StatefulWidget {
  @override
  _NavScreenState createState() => _NavScreenState();
}

class _NavScreenState extends State<NavScreen> {
  final List<Widget> _screens = const [
    HomeScreen(),
    ProjectTab(),
    ProfileTab(),
  ];
  final List<IconData> _icons = [
    LucideIcons.home,
    LucideIcons.folder,
    LucideIcons.user,
  ];
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _icons.length,
      child: Scaffold(
        backgroundColor: Palette.backgroundBody,
        body: IndexedStack(
          index: _selectedIndex,
          children: _screens,
        ),
        bottomNavigationBar: Container(
          decoration:const BoxDecoration(
              color: Palette.white,
              border: Border(top: BorderSide(color: Palette.border, width: 1)),
          ),
          padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
          child: CustomTabBar(
            icons: _icons,
            selectedIndex: _selectedIndex,
            onTap: (index) => setState(() => _selectedIndex = index),
          ),
        ),
      ),
    );
  }
}

