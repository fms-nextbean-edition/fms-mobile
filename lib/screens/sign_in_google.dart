import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fmsmobile/config/palette.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../services/api_service.dart';
import '../services/storage_service.dart';
import '../models/account.dart';

class SignInGoogle extends StatefulWidget {
  const SignInGoogle({super.key});

  @override
  _SignInGoogleState createState() => _SignInGoogleState();
}

enum SignInStatus { initial, loading, success, failure }

class _SignInGoogleState extends State<SignInGoogle> {
  SignInStatus _signInStatus = SignInStatus.initial;
  String? _errorMessage;
  Account? _account;
  
@override
  void initState() {
    super.initState();
     _initializeStorageService();
    _loadUserData();
  }
Future<void> _initializeStorageService() async {
    final prefs = await SharedPreferences.getInstance();
    StorageService().setSharedPreferences(prefs);
  }
  Future<void> _loadUserData() async {
    final storageService = StorageService();
    final account = await storageService.getUserData();
    if (account != null) {
      setState(() {
        _account = account;
      });
    }
  }
  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: ['https://www.googleapis.com/auth/userinfo.profile'],
  );

  // xử lý login
  Future<void> _handleSignIn() async {
    setState(() {
      _signInStatus = SignInStatus.loading;
      _errorMessage = null;
    });

    try {
      final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
      if (googleUser != null) {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;
        final responseGoogle =
            await ApiService.getIdTokenFromAccessToken(googleAuth.accessToken!);
        final idToken = responseGoogle!;
        if (idToken != null) {
          final response = await ApiService.loginGoogle(idToken);
          if (response.statusCode == 200) {
            final responseData = jsonDecode(response.body);
            final data = responseData['data'];
            final accessToken = data['accessToken'];
            final decodedToken = JwtDecoder.decode(accessToken);
            final userId = decodedToken['userId'];
            final responseFreelancer = await ApiService.getFreelancer(userId);
            if (responseFreelancer.statusCode == 200) {
              final freelancerResponse = jsonDecode(responseFreelancer.body);
              final freelancerData = freelancerResponse['data'];
              final account = Account(
                id: freelancerData['id'],
                lastName: freelancerData['lastName'],
                firstName: freelancerData['firstName'],
                gender: freelancerData['gender'],
                dob: DateTime.parse(freelancerData['dateOfBirth']),
                address: freelancerData['address'],
                image: freelancerData['image'] ?? '',
                code: freelancerData['code'],
                status: freelancerData['isDeleted'],
                accessTokenExpiryTime:
                    DateTime.parse(data['accessTokenExpiryTime']),
              );

              final storageService = StorageService();
              await storageService.saveUserData(account);
              setState(() {
                _signInStatus = SignInStatus.success;
                _account = account;
              });
              Navigator.pushNamed(context, '/home');
            }
          } else {
            setState(() {
              _signInStatus = SignInStatus.failure;
              _errorMessage = 'Login failed: ${response.body}';
            });
          }
        } else {
          setState(() {
            _signInStatus = SignInStatus.failure;
            _errorMessage = 'idToken is null';
          });
        }
      } else {
        setState(() {
          _signInStatus = SignInStatus.failure;
          _errorMessage = 'googleUser is null';
        });
      }
    } catch (error) {
      setState(() {
        _signInStatus = SignInStatus.failure;
        _errorMessage = 'Error when trying to sign in: $error';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 64,
                  height: 64,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Palette.greyText,
                  ),
                ),
                if (_account?.image != null)
                  CircleAvatar(
                    radius: 50,
                    backgroundImage: NetworkImage(_account!.image!),
                  ),
              ],
            ),
            const SizedBox(height: 16.0),
            Text(
              'Welcome back',
              textAlign: TextAlign.center,
              style: GoogleFonts.ibmPlexSans(
                fontSize: 24.0,
                height: 1.4,
                fontWeight: FontWeight.w600,
                color: Palette.blackText,
              ),
            ),
            //const SizedBox(height: 2.0),
            Text(
              'Continue as Freelancer',
              textAlign: TextAlign.center,
              style: GoogleFonts.ibmPlexSans(
                fontSize: 16.0,
                height: 1.5,
                fontWeight: FontWeight.w400,
                color: Palette.greyText,
              ),
            ),
            const SizedBox(height: 16.0),
            OutlinedButton.icon(
              style: OutlinedButton.styleFrom(
                side: const BorderSide(color: Palette.border),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                minimumSize: const Size(382, 40),
                padding:
                    const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              ),
              onPressed: _handleSignIn,
              icon: const Icon(
                Icons.circle_outlined,
                color: Color.fromRGBO(39, 39, 42, 1),
                size: 16.0,
              ),
              label: Text(
                'Login with Google',
                style: GoogleFonts.ibmPlexSans(
                  fontSize: 16.0,
                  height: 1.5,
                  fontWeight: FontWeight.w500,
                  color: Palette.blackText,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
