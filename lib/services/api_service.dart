import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class ApiService {
  static Future<http.Response> loginGoogle(String idToken) async {
    final url = Uri.parse(
        '${dotenv.env['API_URL']}/api/v1/authentication/login-google');
    final headers = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    };
    final body = jsonEncode(<String, String>{'idToken': idToken});

    try {
      final response = await http.post(url, headers: headers, body: body);
      print('Response body: ${response.body}');
      return response;
    } catch (error) {
      print('Error during API call: $error');
      rethrow;
    }
  }

  static Future<http.Response> getFreelancer(String id) async {
    final url = Uri.parse('${dotenv.env['API_URL']}/api/v1/freelancer/$id');
    final headers = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    };

    try {
      final response = await http.get(url, headers: headers);
      print('Response body: ${response.body}');
      return response;
    } catch (error) {
      print('Error during API call: $error');
      rethrow;
    }
  }

  static Future<String?> getIdTokenFromAccessToken(String accessToken) async {
    final apiKey = dotenv.env['API_KEY'];
    final url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithIdp?key=$apiKey';
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        'postBody': 'access_token=$accessToken&providerId=google.com',
        'requestUri': 'http://localhost',
        'returnIdpCredential': true,
        'returnSecureToken': true,
      }),
    );

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      return data['idToken'];
    } else {
      print('Failed to get idToken: ${response.body}');
      return null;
    }
  }

// static Future<String?> getIdTokenFromAccessToken(String accessToken) async {
//   print("helo");
//     final response = await http.get(
//       Uri.parse('https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=$accessToken'),
//     );
// print(response.statusCode);
// print(response.body);
//     if (response.statusCode == 200) {
//       final data = jsonDecode(response.body);
//       return data['id_token'];
//     } else {
//       print('Failed to get idToken: ${response.body}');
//       return null;
//     }
//   }

//  static Future<String?> getIdTokenFromGoogle() async {
//   const redirectUri = 'YOUR_REDIRECT_URI';
//   const scope = 'openid';
//   const responseType = 'id_token';

//   final url = Uri.parse('https://accounts.google.com/o/oauth2/v2/auth');
//   final response = await http.post(
//     url,
//     body: {
//       'client_id': dotenv.env['ANDROID_CLIENT_ID'],
//       'redirect_uri': redirectUri,
//       'scope': scope,
//       'response_type': responseType,
//     },
//   );

//   if (response.statusCode == 200) {
//     final data = jsonDecode(response.body);
//     final idToken = data['id_token'];
//     return idToken;
//   } else {
//     print('Failed to get idToken from Google: ${response.body}');
//     return null;
//   }
// }
}
