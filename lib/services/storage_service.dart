import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/account.dart';

class StorageService {
  late SharedPreferences _prefs;

  Future<void> setSharedPreferences(SharedPreferences prefs) async {
    _prefs = prefs;
  }

  Future<void> saveUserData(Account account) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('userData', jsonEncode(account.toJson()));
  }

  Future<Account?> getUserData() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final userDataString = pref.getString('userData');
    if (userDataString != null) {
      return Account.fromJson(jsonDecode(userDataString));
    }
    return null;
  }

  Future<void> clearUserData() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.remove('userData');
  }
}
