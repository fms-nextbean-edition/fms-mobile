import 'package:flutter/material.dart';
import 'package:fmsmobile/config/palette.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lucide_icons/lucide_icons.dart';

class CustomSearchBar extends StatelessWidget {
  final TextEditingController controller;
  final VoidCallback onTap;
  final ValueChanged<String> onChanged;

  const CustomSearchBar({
    Key? key,
    required this.controller,
    required this.onTap,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          border: Border.all(color: Palette.border),
        ),
        child: Row(
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(16, 8, 0, 8),
              child: Icon(
               LucideIcons.search ,
                color: Palette.greyText,
                size: 16.0,
              ),
            ),
            Expanded(
              child: TextField(
                controller: controller,
                onTap: onTap,
                onChanged: onChanged,
                decoration: InputDecoration(
                  hintText: 'Search',
                  hintStyle: GoogleFonts.ibmPlexSans(
                      fontSize: 16.0,
                      height: 1.5,
                      fontWeight: FontWeight.w400,
                      color: Palette.blackText,
                    ),
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.symmetric( horizontal: 8.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
