import 'package:flutter/material.dart';
import 'package:fmsmobile/config/palette.dart';

class CustomTabBar extends StatelessWidget {
  final List<IconData> icons;
  final int selectedIndex;
  final Function(int) onTap;
  final bool isBottomIndicator;

  const CustomTabBar({
    Key? key,
    required this.icons,
    required this.selectedIndex,
    required this.onTap,
    this.isBottomIndicator = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
      indicatorPadding: EdgeInsets.zero,
      indicator: BoxDecoration(
        border: isBottomIndicator
            ? const Border(
                bottom: BorderSide(
                  color: Palette.green,
                  width: 3.0,
                ),
              )
            : const Border(
                top: BorderSide(
                  color: Palette.white,
                  //width: 0,
                ),
              ),
      ),
      tabs: icons
          .asMap()
          .map((i, e) => MapEntry(
                i,
                Container(
                  height: 72.0,
                  padding: const EdgeInsets.symmetric(
                    vertical: 24.0,
                    horizontal: 0,
                  ),
                  child: Tab(
                    icon: SizedBox(
                      width: 24.0,
                      height: 24.0,
                      child: Icon(
                        e,
                        color: i == selectedIndex
                            ? Palette.green
                            : Palette.blackText,
                        size: 24.0,
                      ),
                    ),
                  ),
                ),
              ))
          .values
          .toList(),
      onTap: onTap,
    );
  }
}
