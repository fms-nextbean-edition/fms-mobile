import 'package:flutter/material.dart';
import 'package:fmsmobile/models/models.dart';
import 'package:fmsmobile/widgets/profile_avatar.dart';

class UserCard extends StatelessWidget {
  final Account account;

  const UserCard({
    Key? key,
    required this.account,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ProfileAvatar(imageUrl: account.image!),
          const SizedBox(width: 6.0),
          Flexible(
            child: Text(
              "${account.firstName}" "${account.lastName}",
              style: const TextStyle(fontSize: 16.0),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}